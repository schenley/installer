<?php

/**
 * Part of the Installer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Installer
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

Route::group(['prefix' => 'install', 'namespace' => 'Synergy\Installer\Http\Controllers'], function()
{
   get('/', ['as' => 'installer', 'uses' => 'InstallerController@index']);
});
