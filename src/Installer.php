<?php

namespace Schenley\Installer;

use Illuminate\Container\Container;
use Schenley\Config\Repository;

/**
 * Part of the Installer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Installer
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */
 
 class Installer
 {

     /**
      * Illuminate Container instance
      *
      * @var \Illuminate\Container\Container
      */
     protected $container;

     /**
      * Array containing packages that are required
      * for installation.
      *
      * @var array
      */
     protected $requiredPackages = [
         'schenley/config',
         'schenley/modules',
         'schenley/platform',
         'cartalyst/sentinel'
     ];

     /**
      * @param \Illuminate\Container\Container $container
      */
     public function __construct(Container $container)
     {
         $this->container = $container;
     }

     /**
      * Installs the Synergy Platform
      *
      * @param bool $testing
      */
     public function install($testing = false, $adminUser = [])
     {
         // fire off a before event to allow developers to hook
         // into the install process

         $this->runMigrations();

         $this->installModules();

         $this->overrideConfigInstance();

         $this->updateInstalledVersion();

         if ( ! is_null($adminUser)) {
             $this->createAdminUser();
         }
     }

     /**
      * Runs the required migrations
      */
     protected function runMigrations()
     {
         $this->prepareMigrationRepository();

         $this->migrateDefaultMigrations();

         $this->migrateRequiredPackages();
     }

     /**
      * Ensures we're ready to run the migrations.
      *
      * @return void
      */
     protected function prepareMigrationRepository()
     {
         try
         {
             $this->container['migration.repository']->getLast();
         }
         catch (\Exception $e)
         {
             $this->container['migration.repository']->createRepository();
         }
     }

     /**
      * Runs the migrations contained in the application's
      * default migration path.
      *
      * Currently we are using Laravel's built in auth, so
      * we need these to run.
      */
     protected function migrateDefaultMigrations()
     {
         // run default migrations
         \Artisan::call('migrate');
     }

     /**
      * Migrates the required packages
      */
     protected function migrateRequiredPackages()
     {
         $this->prepareMigrationRepository();

         $vendorPath = base_path('vendor');
         $workbenchPath = base_path('workbench');

         foreach ($this->requiredPackages as $package) {

             $path = "{$vendorPath}/{$package}/database/migrations";

             if ( ! $this->container['files']->exists($path)) {
                 $path = "{$workbenchPath}/{$package}/database/migrations";
             }

             $this->container['migrator']->run($path);
         }
     }

     /**
      * Installs all of the modules that are currently present
      */
     protected function installModules()
     {
         $modules = $this->container['modules'];

         $modules->findAndRegister(true);

         // Flush the Cache
         $this->container['cache']->flush();

         // loop through each of the modules and install
         // and enable them.
         foreach($modules->all() as $module) {

             $module->install();

             $module->enable();
         }
     }

     /**
      * Overrides the config instance.
      *
      * @return void
      */
     protected function overrideConfigInstance()
     {
         $repository = new Repository([], $this->container['cache']);

         $old = $this->container['config']->all();

         foreach ($old as $key => $value) {
             $repository->set($key, $value);
         }

         $this->container->instance('config', $repository);
     }

     /**
      * Updates the installed version of the Synergy Platform.
      */
     protected function updateInstalledVersion()
     {
         config()->persist('platform.version', app('platform')->getCodebaseVersion());
     }

     /**
      * Creates a new Admin User
      */
     protected function createAdminUser()
     {
         $data = [
             'first_name' => 'Admin',
             'last_name' => 'User',
             'email' => 'admin@myschool.org',
             'password' => 'welcome'
         ];

         app('sentinel')->create($data);
     }
 }
