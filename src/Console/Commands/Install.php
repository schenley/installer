<?php

namespace Schenley\Installer\Console\Commands;

use Illuminate\Console\Command;
use Schenley\Installer\Installer;
use Schenley\Modules\Modules;

/**
 * Part of the Installer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Installer
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */


class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schenley:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs the Schenley Learning Platform';

    /**
     * Instance of the Synergy Installer
     *
     * @var \Synergy\Installer\Installer
     */
	protected $installer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Installer $installer, Modules $modules)
    {
        parent::__construct();

		$this->installer = $installer;

        $this->modules = $modules;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->installer->install();

        $this->info('The Schenley Learning Platform has been installed!');

    }
}
