<?php

namespace Schenley\Installer\Providers;

use Schenley\Support\ServiceProvider;

/**
 * Part of the Installer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Installer
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */
 
 class InstallerServiceProvider extends ServiceProvider
 {

     /**
      * Boot the packages resources and assets
      */
     public function boot()
     {
         $this->bootResources();

         require __DIR__ . '/../Http/routes.php';
     }

     /**
      * Register the service provider.
      *
      * @return void
      */
     public function register()
     {
         $this->commands([
             'Schenley\Installer\Console\Commands\Install'
         ]);
     }

     /**
      * Boots the packages resources
      */
     public function bootResources()
     {
         $viewsPath = __DIR__ . '/../../resources/views';

         $this->loadViewsFrom($viewsPath, 'installer');

         $this->publishes([
             $viewsPath => base_path('resources/views/vendor/installer'),
         ], 'views');

         $this->publishes([
             __DIR__ . '/../../resources/assets' => public_path('vendor/installer'),
         ], 'public');
     }

 }
