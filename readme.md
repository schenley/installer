## Installer

Installer package for the Schenley Learning Platform

## API Documentation

API documentation can be found in the api directory.

### License

This package is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
